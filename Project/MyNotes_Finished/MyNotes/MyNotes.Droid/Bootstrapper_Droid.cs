﻿
using MyNotes.Core.Interfaces;
using MyNotes.Droid.Helpers;
using MyNotes.Utils;

namespace MyNotes.Droid
{
    public class Bootstrapper_Droid
    {
        public static void Initialize()
        {
            InstanceFactory.Initialize();

            App.RegisterTypes();
            RegisterTypes();

            InstanceFactory.Build();
        }

        public static void RegisterTypes()
        {
            InstanceFactory.Register<ILocalFileSystemHelper, LocalFileSystemHelper_Droid>();
        }
    }
}
