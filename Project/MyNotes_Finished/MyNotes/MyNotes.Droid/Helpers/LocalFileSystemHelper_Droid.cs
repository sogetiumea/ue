using System;
using System.Collections.Generic;
using System.Linq;
using MyNotes.Core.Interfaces;

namespace MyNotes.Droid.Helpers
{
    class LocalFileSystemHelper_Droid : ILocalFileSystemHelper
    {
        private string GetStoragePath()
        {
            return System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
        }

        public string GetLocalPath(params string[] paths)
        {
            string storagePath = GetStoragePath();
            List<string> pathList = paths.ToList();
            pathList.Insert(0, storagePath);
            return System.IO.Path.Combine(pathList.ToArray());
        }

        public bool LocalPathExists(params string[] paths)
        {
            string path = GetLocalPath(paths);
            if (System.IO.Directory.Exists(path))
            {
                return true;
            }
            return System.IO.File.Exists(path);
        }

        public void CreateLocalFolder(params string[] paths)
        {
            string path = GetLocalPath(paths);
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
        }

        public bool DeleteLocalPath(params string[] paths)
        {
            string path = GetLocalPath(paths);
            if (System.IO.Directory.Exists(path))
            {
                System.IO.Directory.Delete(path, true);
                return true;
            }
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
                return true;
            }
            return false;
        }
    }
}