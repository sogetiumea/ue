﻿
using MyNotes.Core.Interfaces;
using MyNotes.iOS.Helpers;
using MyNotes.Utils;

namespace MyNotes.iOS
{
    public class Bootstrapper_iOS
    {
        public static void Initialize()
        {
            InstanceFactory.Initialize();

            App.RegisterTypes();
            RegisterTypes();

            InstanceFactory.Build();
        }

        public static void RegisterTypes()
        {
            InstanceFactory.Register<ILocalFileSystemHelper, LocalFileSystemHelper_iOS>();
        }
    }
}
