﻿using Xamarin.Forms;
using MyNotes.ViewModels;
using MyNotes.Utils;
using MyNotes.Core.Managers;
using MyNotes.Core.Interfaces;
using MyNotes.Core.Repositories;
using MyNotes.Views;
using MyNotes.Helpers;
using MyNotes.Core.Helpers;
using MyNotes.Converters;

namespace MyNotes
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //MainPage = new ContentPage
            //{
            //    Content = new StackLayout
            //    {
            //        VerticalOptions = LayoutOptions.Center,
            //        Children = {
            //            new Label {
            //                HorizontalTextAlignment = TextAlignment.Center,
            //                Text = "Welcome to Xamarin Forms!"
            //            }
            //        }
            //    }
            //};

            RegisterViews();

            var page = ViewFactory.Resolve<NoteListViewModel>();
            MainPage = new NavigationPage(page);
        }

        public static void RegisterTypes()
        {
            InstanceFactory.Register<INoteManager, NoteManager>();
            InstanceFactory.Register<IMessageHelper, MessageHelper>();
            InstanceFactory.Register<INoteRepository, SQLNoteRepository>();
            //InstanceFactory.Register<INoteRepository, InMemoryNoteRepository>(singelton: true);
            InstanceFactory.Register<IDatabaseHelper, DatabaseHelper>();
        }

        private void RegisterViews()
        {
            ViewFactory.Initialize();
            ViewFactory.Register<NoteListViewModel, NoteListPage>();
            ViewFactory.Register<EditNoteViewModel, EditNotePage>();
        }


        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
