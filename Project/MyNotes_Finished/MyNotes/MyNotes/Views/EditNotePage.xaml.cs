﻿using MyNotes.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MyNotes.Views
{
    public partial class EditNotePage : ContentPage
    {
        public EditNotePage()
        {
            InitializeComponent();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            var viewModel = BindingContext as EditNoteViewModel;
            if (viewModel != null)
            {
                viewModel.OnDisappearing();
            }
        }
    }
}
