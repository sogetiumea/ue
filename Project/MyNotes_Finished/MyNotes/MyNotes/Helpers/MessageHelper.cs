﻿using MyNotes.Core.Interfaces;
using System;
using Xamarin.Forms;

namespace MyNotes.Helpers
{
    public class MessageHelper : IMessageHelper
    {
        public void Subscribe<TSource, TData>(object subscriber, string message, Action<TSource, TData> callback) where TSource : class
        {
            MessagingCenter.Subscribe<TSource, TData>(subscriber, message, callback);
        }

        public void Send<TSource, TData>(TSource source, string message, TData data) where TSource : class
        {
            MessagingCenter.Send<TSource, TData>(source, message, data);
        }

        public void Unsubscribe<TSource, TData>(object subscriber, string message) where TSource : class
        {
            MessagingCenter.Unsubscribe<TSource, TData>(subscriber, message);
        }

        public void Subscribe<TSource>(object subscriber, string message, Action<TSource> callback) where TSource : class
        {
            MessagingCenter.Subscribe<TSource>(subscriber, message, callback);
        }

        public void Send<TSource>(TSource source, string message) where TSource : class
        {
            MessagingCenter.Send<TSource>(source, message);
        }

        public void Unsubscribe<TSource>(object subscriber, string message) where TSource : class
        {
            MessagingCenter.Unsubscribe<TSource>(subscriber, message);
        }
    }
}
