﻿using MyNotes.Core.Interfaces;
using MyNotes.Core.Model;
using MyNotes.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Acr.UserDialogs;

namespace MyNotes.ViewModels
{
    public class EditNoteViewModel : BaseViewModel
    {
        public EditNoteViewModel()
        {
            this.noteManager = InstanceFactory.Resolve<INoteManager>();
        }

        public void OnDisappearing()
        {
            if (editNote == null)
            {
                return;
            }

            if (Text != editNote.Text)
            {
                editNote.Text = Text;
                editNote.LastModified = DateTime.Now;
                noteManager.SaveNote(editNote);
            }
        }

        private async Task DeleteNote()
        {
            if (await UserDialogs.Instance.ConfirmAsync("Are you sure that you want to delete this note?", okText:"Yes", cancelText:"No"))
            {
                await noteManager.DeleteNote(editNote);
                editNote = null;
                await Navigation.PopAsync();
            }
        }

        private INoteManager noteManager;

        private Note editNote;
        public Note EditNote
        {
            get
            {
                return editNote;
            }
            set
            {
                editNote = value;
                Text = editNote.Text;
            }
        }

        public string Text { get; set; }

        private ICommand deleteNoteCommand;
        public ICommand DeleteNoteCommand
        {
            get { return deleteNoteCommand ?? (deleteNoteCommand = new Command(async () => await DeleteNote())); }
        }

    }
}
