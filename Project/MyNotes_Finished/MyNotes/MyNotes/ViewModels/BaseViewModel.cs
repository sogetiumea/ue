﻿
using PropertyChanged;
using Xamarin.Forms;

namespace MyNotes.ViewModels
{
    [ImplementPropertyChanged]
    public class BaseViewModel
    {
        public INavigation Navigation { get; set; }
    }
}
