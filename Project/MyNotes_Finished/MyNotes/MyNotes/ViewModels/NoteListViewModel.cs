﻿using MyNotes.Core.Interfaces;
using MyNotes.Core.Model;
using MyNotes.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MyNotes.ViewModels
{
    public class NoteListViewModel : BaseViewModel
    {

        public NoteListViewModel()
        {
            noteManager = InstanceFactory.Resolve<INoteManager>();
            messageHelper = InstanceFactory.Resolve<IMessageHelper>();

            messageHelper.Subscribe<INoteManager>(this, Messages.NotesUpdatedMessage, async (sender) => { await UpdateNoteList(); });

            Device.BeginInvokeOnMainThread(async () => 
            {
                await UpdateNoteList();
            });
        }


        private async Task UpdateNoteList()
        {

            var notes = await noteManager.GetNotes();

            var items = new List<NoteListItem>();
            foreach (var note in notes)
            {
                items.Add(new NoteListItem()
                {
                    Note = note
                });
            }

            NoteList = items;
        }

        private async Task NoteSelected(Note note)
        {
            var viewModel = new EditNoteViewModel();
            viewModel.EditNote = note;
            var page = ViewFactory.Resolve<EditNoteViewModel>(viewModel);
            await Navigation.PushAsync(page);

            SelectedNote = null;
        }

        private async Task NewNote()
        {
            var viewModel = new EditNoteViewModel();
            viewModel.EditNote = new Note();
            var page = ViewFactory.Resolve<EditNoteViewModel>(viewModel);
            await Navigation.PushAsync(page);
        }

        private INoteManager noteManager;
        private IMessageHelper messageHelper;

        private ICommand newNoteCommand;
        public ICommand NewNoteCommand
        {
            get { return newNoteCommand ?? (newNoteCommand = new Command(async () => await NewNote())); }
        }

        private NoteListItem selectedNote;
        public NoteListItem SelectedNote
        {
            get
            {
                return selectedNote;
            }

            set
            {
                selectedNote = value;
                if (selectedNote != null)
                {
                    Device.BeginInvokeOnMainThread(async () =>  {
                        await NoteSelected(selectedNote.Note);
                    });
                }
            }
        }

        public List<NoteListItem> NoteList { get; set; }
    }

    public class NoteListItem
    {
        public Note Note { get; set; }
    }
}
