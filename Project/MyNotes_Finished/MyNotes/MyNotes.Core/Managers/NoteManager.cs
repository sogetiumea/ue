﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyNotes.Core.Interfaces;
using MyNotes.Core.Model;

namespace MyNotes.Core.Managers
{
    public class NoteManager : INoteManager
    {

        public NoteManager(INoteRepository noteRepository, IMessageHelper messageHelper)
        {
            this.noteRepository = noteRepository;
            this.messageHelper = messageHelper;
        }

        public async Task<List<Note>> GetNotes()
        {
            return await noteRepository.GetNotes();
        }

        public async Task DeleteNote(Note note)
        {
            await noteRepository.DeleteNote(note);
            SendNotesUpdated();
        }

        public async Task SaveNote(Note note)
        {
            await noteRepository.SaveNote(note);
            SendNotesUpdated();
        }

        private void SendNotesUpdated()
        {
            messageHelper.Send<INoteManager>(this, Messages.NotesUpdatedMessage);
        }

        private INoteRepository noteRepository;
        private IMessageHelper messageHelper;
    }
}
