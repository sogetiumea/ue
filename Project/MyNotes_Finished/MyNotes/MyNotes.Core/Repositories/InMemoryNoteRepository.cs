﻿using MyNotes.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyNotes.Core.Model;

namespace MyNotes.Core.Repositories
{
    public class InMemoryNoteRepository : INoteRepository
    {

        public InMemoryNoteRepository()
        {
            notes = new List<Note>();        
        }

        public async Task DeleteNote(Note note)
        {
            notes.Remove(note);
            await Task.FromResult<int>(0);
        }

        public async Task<List<Note>> GetNotes()
        {
            List<Note> lst = new List<Note>(notes);
            return await Task.FromResult<List<Note>>(lst);
        }

        public async Task SaveNote(Note note)
        {
            if (note.Id == Guid.Empty)
            {
                note.Id = Guid.NewGuid();
            }

            notes.Remove(note);
            notes.Insert(0, note);
            await Task.FromResult<int>(0);
        }

        private List<Note> notes;

    }
}
