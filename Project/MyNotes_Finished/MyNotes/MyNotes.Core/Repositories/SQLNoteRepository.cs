﻿using MyNotes.Core.Interfaces;
using MyNotes.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyNotes.Core.Repositories
{
    public class SQLNoteRepository : INoteRepository
    {

        public SQLNoteRepository(IDatabaseHelper databaseHelper)
        {
            this.db = databaseHelper;

            Task.Run(async () => {
                await db.MainConnection.CreateTableAsync<Note>();
            }).Wait();

        }

        public async Task SaveNote(Note note)
        {
            if (note.Id == Guid.Empty)
            {
                note.Id = Guid.NewGuid();
            }

            await db.MainConnection.InsertOrReplaceAsync(note);
        }

        public async Task DeleteNote(Note note)
        {
            await db.MainConnection.DeleteAsync(note);
        }

        public async Task<List<Note>> GetNotes()
        {
            return await db.MainConnection.QueryAsync<Note>("SELECT * from Note ORDER BY LastModified DESC;");
        }

        private IDatabaseHelper db;

    }
}
