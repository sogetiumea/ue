﻿
using System;
using MyNotes.Core.Utils;
using SQLite;

namespace MyNotes.Core.Model
{
    public class Note : IEquatable<Note>
    {
        [PrimaryKey]
        public Guid Id { get; set; }
        public string Text { get; set; }
        public DateTime LastModified { get; set; }

        [Ignore]
        public string Title
        {
            get
            {
                return Text.FirstLineOrDefault("(Empty note)");
            }
        }

        // --- Equals--- 

        public override bool Equals(object obj)
        {
            Note note = obj as Note;
            if (note == null)
            {
                return false;
            }
            return Equals(note);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public bool Equals(Note other)
        {
            return (this.Id == other.Id);
        }
    }
}
