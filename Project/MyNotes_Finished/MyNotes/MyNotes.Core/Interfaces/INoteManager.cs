﻿using MyNotes.Core.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace MyNotes.Core.Interfaces
{
    public interface INoteManager
    {
        Task<List<Note>> GetNotes();
        Task SaveNote(Note note);
        Task DeleteNote(Note note);
    }
}
