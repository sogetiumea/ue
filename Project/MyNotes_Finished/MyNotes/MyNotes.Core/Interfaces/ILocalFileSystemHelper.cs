﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNotes.Core.Interfaces
{
    public interface ILocalFileSystemHelper
    {
        string GetLocalPath(params string[] paths);
        bool LocalPathExists(params string[] paths);
        void CreateLocalFolder(params string[] paths);
        bool DeleteLocalPath(params string[] paths);
    }
}
