﻿using System;
using SQLite;
using System.Diagnostics;

namespace MyNotes.Core.Interfaces
{
    public interface IDatabaseHelper
    {
        SQLiteAsyncConnection MainConnection { get; }

        SQLiteAsyncConnection CreateDatabase(string databaseName);
    }
}
