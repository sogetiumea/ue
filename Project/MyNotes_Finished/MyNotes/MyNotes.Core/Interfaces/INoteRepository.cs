﻿using MyNotes.Core.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyNotes.Core.Interfaces
{
    public interface INoteRepository
    {
        Task<List<Note>> GetNotes();
        Task SaveNote(Note note);
        Task DeleteNote(Note note);
    }
}
