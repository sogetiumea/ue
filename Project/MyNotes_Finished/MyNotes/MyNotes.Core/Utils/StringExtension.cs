﻿using System;


namespace MyNotes.Core.Utils
{
    public static class StringExtension
    {
        public static string FirstLineOrDefault(this string str, string defaultString)
        {
            var text = str;

            if (text == null)
            {
                text = "";
            }

            string[] lines = text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            if (lines.Length == 0 || (text = lines[0].Trim()).Length == 0)
            {
                return defaultString;
            }

            return text;

        }
    }
}
