﻿using MyNotes.Core.Interfaces;
using SQLite;
using System;
using System.Diagnostics;

namespace MyNotes.Core.Helpers
{
    public class DatabaseHelper : IDatabaseHelper
    {
        public DatabaseHelper(ILocalFileSystemHelper fileSystem)
        {
            this.fileSystem = fileSystem;
        }

        public SQLiteAsyncConnection CreateDatabase(string databaseName)
        {
            try
            {
                string databaseFolderPath = fileSystem.GetLocalPath("Database");
                string databasePath = fileSystem.GetLocalPath("Database", databaseName + ".sqlite3");

                if (!fileSystem.LocalPathExists(databaseFolderPath))
                {
                    fileSystem.CreateLocalFolder(databaseFolderPath);
                }

                SQLiteAsyncConnection con = new SQLiteAsyncConnection(databasePath);

                return con;
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception: " + e);
            }
            return null;
        }

        private volatile SQLiteAsyncConnection mainConection = null;
        public SQLiteAsyncConnection MainConnection
        {
            get
            {
                return mainConection ?? (mainConection = CreateDatabase("main"));
            }
        }

        private ILocalFileSystemHelper fileSystem;
    }

}
