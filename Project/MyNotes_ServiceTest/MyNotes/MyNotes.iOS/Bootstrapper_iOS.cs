﻿
using MyNotes.Utils;
using MyNotes.Core.Interfaces;

namespace MyNotes.iOS
{
    public class Bootstrapper_iOS
    {
        public static void Initialize()
        {
            InstanceFactory.Initialize();

            App.RegisterTypes();
            RegisterTypes();

            InstanceFactory.Build();
        }

        public static void RegisterTypes()
        {
        }
    }
}
