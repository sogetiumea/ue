﻿using MyNotes.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyNotes.Core.Models;

namespace MyNotes.Core.Managers
{
    public class PrizeManager : IPrizeManager
    {
        public PrizeManager(IPrizeService prizeService)
        {
            this.prizeService = prizeService;
        }

        public async Task<List<Prize>> GetPrizes()
        {
            return await prizeService.GetPrizes();
        }

        private IPrizeService prizeService;
    }
}
