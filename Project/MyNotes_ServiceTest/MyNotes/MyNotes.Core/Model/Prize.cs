﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MyNotes.Core.Models
{
    public class Prize
    {
        [JsonProperty(PropertyName = "year")]
        public string Year { get; set; }

        [JsonProperty(PropertyName = "category")]
        public string Category { get; set; }
        
        [JsonProperty(PropertyName = "laureates")]
        public List<Laureate> Laureates { get; set; }
        
        [JsonProperty(PropertyName = "overallMotivation")]
        public string OverallMotivation { get; set; }
    }
}
