﻿using Newtonsoft.Json;
using System;

namespace MyNotes.Core.Models
{
    public class Laureate
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "firstname")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "surname")]
        public string Surname { get; set; }

        [JsonProperty(PropertyName = "motivation")]
        public string Motivation { get; set; }

        [JsonProperty(PropertyName = "share")]
        public string Share { get; set; }
    }
}
