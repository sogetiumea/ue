﻿using MyNotes.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNotes.Core.Interfaces
{
    public interface IPrizeManager
    {
        Task<List<Prize>> GetPrizes();
    }
}
