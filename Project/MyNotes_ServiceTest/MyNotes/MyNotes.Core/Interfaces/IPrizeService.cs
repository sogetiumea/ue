﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MyNotes.Core.Models;

namespace MyNotes.Core.Interfaces
{
    public interface IPrizeService
    {
        Task<List<Prize>> GetPrizes();
    }
}