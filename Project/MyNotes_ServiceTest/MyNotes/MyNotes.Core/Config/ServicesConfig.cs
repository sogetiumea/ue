﻿namespace MyNotes.Core.Config
{
    public class ServicesConfig
    {
        public const string WebServiceBaseAddress = "http://api.nobelprize.org/v1/";        

        public const string GetPrizeService = "prize.json";
    }
}
