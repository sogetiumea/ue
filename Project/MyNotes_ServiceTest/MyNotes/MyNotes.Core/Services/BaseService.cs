﻿using MyNotes.Core.Config;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MyNotes.Core.Services
{
    public class BaseService
    {
        protected Uri GetServiceUri(string service)
        {
            return new Uri(ServicesConfig.WebServiceBaseAddress + service);
        }

        private string GetParameterString(Dictionary<string, string> parameters)
        {
            if (parameters == null || parameters.Count == 0)
            {
                return string.Empty;
            }
            var list = new List<string>();
            foreach (var item in parameters)
            {
                list.Add(item.Key + "=" + item.Value);
            }
            return "?" + string.Join("&", list);
        }

        protected async Task<T> GetFromService<T>(string service, Dictionary<string, string> parameters = null)
        {
            ResultException = null;
            ResultStatusCode = null;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));

                    HttpResponseMessage response = null;

                    response = await client.GetAsync(GetServiceUri(service) + GetParameterString(parameters));

                    string test = (GetServiceUri(service) + GetParameterString(parameters));

                    ResultStatusCode = response.StatusCode;

                    string data = null;

                    if (ResultStatusCode == HttpStatusCode.OK)
                    {
                        data = await response.Content.ReadAsStringAsync();

                        var deserializerSettings = new JsonSerializerSettings()
                        {
                            DateFormatHandling = DateFormatHandling.IsoDateFormat,
                            DateParseHandling = DateParseHandling.DateTimeOffset,
                            NullValueHandling = NullValueHandling.Ignore
                        };

                        return JsonConvert.DeserializeObject<T>(data, deserializerSettings);
                    }
                    else if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        Debug.WriteLine("Unauthorized");
                    }
                    else
                    {
                        Debug.WriteLine("Response code:" + response.StatusCode.ToString());
                    }

                    return default(T);
                }
            }
            catch (Exception ex)
            {
                ResultException = ex;
                Debug.WriteLine("PostToService exception: " + ex);
                return default(T);
            }
        }


        protected async Task<T> PostToService<T>(string service, object postObject)
        {
            ResultException = null;
            ResultStatusCode = null;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));

                    HttpResponseMessage response = null;

                    string postString = JsonConvert.SerializeObject(postObject);

                    response = await client.PostAsync(GetServiceUri(service), new StringContent(postString, Encoding.UTF8, "application/json"));

                    ResultStatusCode = response.StatusCode;

                    string data = null;

                    if (ResultStatusCode == HttpStatusCode.OK)
                    {
                        data = await response.Content.ReadAsStringAsync();

                        var deserializerSettings = new JsonSerializerSettings()
                        {
                            DateFormatHandling = DateFormatHandling.IsoDateFormat,
                            DateParseHandling = DateParseHandling.DateTimeOffset,
                        };

                        return JsonConvert.DeserializeObject<T>(data, deserializerSettings);
                    }
                    else if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        Debug.WriteLine("Unauthorized");
                    }
                    else
                    {
                        Debug.WriteLine("Response code:" + response.StatusCode.ToString());
                    }
                    return default(T);
                }
            }
            catch (Exception ex)
            {
                ResultException = ex;
                Debug.WriteLine("PostToService exception: " + ex);
                return default(T);
            }
        }

        protected async Task<string> PostToService(string service, object postObject)
        {
            ResultException = null;
            ResultStatusCode = null;

            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));

                    HttpResponseMessage response = null;

                    string postString = JsonConvert.SerializeObject(postObject);

                    response = await client.PostAsync(GetServiceUri(service), new StringContent(postString, Encoding.UTF8, "application/json"));

                    ResultStatusCode = response.StatusCode;

                    string data = null;

                    if (ResultStatusCode == HttpStatusCode.OK)
                    {
                        data = await response.Content.ReadAsStringAsync();
                        return data;
                    }
                    else if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        Debug.WriteLine("Unauthorized");
                    }
                    else
                    {
                        Debug.WriteLine("Response code:" + response.StatusCode.ToString());
                    }
                    return default(string);
                }
            }
            catch (Exception ex)
            {
                ResultException = ex;
                Debug.WriteLine("PostToService exception: " + ex);
                return default(string);
            }
        }

        public HttpStatusCode? ResultStatusCode;
        public Exception ResultException;
    }
}
