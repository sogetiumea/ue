﻿using MyNotes.Core.Config;
using System.Collections.Generic;
using System.Threading.Tasks;
using MyNotes.Core.Models;
using Newtonsoft.Json;
using MyNotes.Core.Interfaces;

namespace MyNotes.Core.Services
{
    public class PrizeService : BaseService, IPrizeService
    {
        public async Task<List<Prize>> GetPrizes()
        {
            PrizesRootObject result = await GetFromService<PrizesRootObject>(ServicesConfig.GetPrizeService);
            return result.PrizeList;
        }
    }

    public class PrizesRootObject
    {
        [JsonProperty(PropertyName = "prizes")]
        public List<Prize> PrizeList { get; set; }
    }

}
