﻿
using Xamarin.Forms;

using MyNotes.ViewModels;
using MyNotes.Views;
using MyNotes.Utils;
using MyNotes.Core.Managers;
using MyNotes.Core.Interfaces;
using MyNotes.Core.Services;

namespace MyNotes
{
    public class App : Application
    {
        public App()
        {
            RegisterViews();
  
            var page = ViewFactory.Resolve<TestViewModel>();
            MainPage = new NavigationPage(page);
        }

        public static void RegisterTypes()
        {
            InstanceFactory.Register<IPrizeManager, PrizeManager>();
            InstanceFactory.Register<IPrizeService, PrizeService>();
        }

        private void RegisterViews()
        {
            ViewFactory.Initialize();

            ViewFactory.Register<TestViewModel, TestPage>();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
