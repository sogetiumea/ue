﻿
using MyNotes.Core.Interfaces;
using MyNotes.Core.Models;
using MyNotes.Utils;
using MyNotes.Views;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MyNotes.ViewModels
{    
    public class TestViewModel : BaseViewModel
    {
        public TestViewModel()
        {
        }

        private async Task ButtonClicked()
        {
            IPrizeManager prizeManager = InstanceFactory.Resolve<IPrizeManager>();
            List<Prize> prizeList = await prizeManager.GetPrizes();

            foreach (var prize in prizeList)
            {
                Debug.WriteLine(prize.Year + " " + prize.Category);
            }
        }

        private ICommand buttonClickedCommand;
        public ICommand ButtonClickedCommand
        {
            get
            {            
                return buttonClickedCommand ?? (buttonClickedCommand  = new Command(async () => await ButtonClicked()));
            }
        }
    }
}
