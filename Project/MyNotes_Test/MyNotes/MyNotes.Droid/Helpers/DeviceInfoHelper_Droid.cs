﻿using Android.OS;
using MyNotes.Core.Interfaces;

namespace MyNotes.Droid.Helpers
{
    public class DeviceInfoHelper_Droid : IDeviceInfoHelper
    {
        public string GetDeviceInfo()
        {
            return "Android (" + Build.VERSION.Release + ")";
        }
    }
}
