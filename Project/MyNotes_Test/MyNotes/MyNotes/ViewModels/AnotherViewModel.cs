﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyNotes.ViewModels
{
    public class AnotherViewModel : BaseViewModel
    {
        public string AnotherMessage { get; set; }
    }
}
