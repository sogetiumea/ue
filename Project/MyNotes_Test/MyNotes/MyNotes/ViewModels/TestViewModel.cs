﻿
using MyNotes.Core.Interfaces;
using MyNotes.Utils;
using MyNotes.Views;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace MyNotes.ViewModels
{    
    public class TestViewModel : BaseViewModel
    {
        public TestViewModel()
        {
            HelloMessage = "Hello from ViewModel";
        }

        private async Task ButtonClicked()
        {
            //HelloMessage = "Button clicked!";

            //var page = new AnotherPage();
            //var viewModel = new AnotherViewModel();
            //viewModel.AnotherMessage = "This is another message.";
            //page.BindingContext = viewModel;
            //viewModel.Navigation = page.Navigation;

            var viewModel = new AnotherViewModel();
            //viewModel.AnotherMessage = "This is another message.";
            var messageManager = InstanceFactory.Resolve<IMessageManager>();
            viewModel.AnotherMessage = messageManager.GetMessage();
            var page = ViewFactory.Resolve<AnotherViewModel>(viewModel);

            await Navigation.PushAsync(page);
        }

        public string HelloMessage { get; set; }

        private ICommand buttonClickedCommand;
        public ICommand ButtonClickedCommand
        {
            get
            {            
                return buttonClickedCommand ?? (buttonClickedCommand  = new Command(async () => await ButtonClicked()));
            }
        }
    }
}
