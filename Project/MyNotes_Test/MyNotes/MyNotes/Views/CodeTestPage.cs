﻿using Xamarin.Forms;

namespace MyNotes.Views
{
    public class CodeTestPage : ContentPage
    {
        public CodeTestPage()
        {
            Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.Center,
                Children = {
                        new Label {
                            HorizontalTextAlignment = TextAlignment.Center,
                            Text = "Hello from Code!"
                        }
                    }
            };
        }
    }
}
