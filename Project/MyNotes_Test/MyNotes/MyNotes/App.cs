﻿
using Xamarin.Forms;

using MyNotes.ViewModels;
using MyNotes.Views;
using MyNotes.Utils;
using MyNotes.Core.Managers;
using MyNotes.Core.Interfaces;

namespace MyNotes
{
    public class App : Application
    {
        public App()
        {
            //MainPage = new ContentPage
            //{
            //    Content = new StackLayout
            //    {
            //        VerticalOptions = LayoutOptions.Center,
            //        Children = {
            //            new Label {
            //                HorizontalTextAlignment = TextAlignment.Center,
            //                Text = "Welcome to Xamarin Forms!"
            //            }
            //        }
            //    }
            //};

            //MainPage = new CodeTestPage();

            //var page = new TestPage();
            //var viewModel = new TestViewModel();
            //page.BindingContext = viewModel;
            //viewModel.Navigation = page.Navigation;

            RegisterViews();

            var page = ViewFactory.Resolve<TestViewModel>();
            MainPage = new NavigationPage(page);
        }

        public static void RegisterTypes()
        {
            InstanceFactory.Register<IMessageManager, MessageManager>();
        }

        private void RegisterViews()
        {
            ViewFactory.Initialize();

            ViewFactory.Register<TestViewModel, TestPage>();
            ViewFactory.Register<AnotherViewModel, AnotherPage>();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
