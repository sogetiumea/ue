﻿using System;
using MyNotes.Core.Interfaces;

namespace MyNotes.Core.Managers
{
    public class MessageManager : IMessageManager
    {
        public MessageManager(IDeviceInfoHelper deviceInfoHelper)
        {
            this.deviceInfoHelper = deviceInfoHelper;
        }

        public string GetMessage()
        {
            return "Hello from " + deviceInfoHelper.GetDeviceInfo() + "!";
        }

        private IDeviceInfoHelper deviceInfoHelper;
    }
}
