﻿
using MyNotes.Utils;
using MyNotes.Core.Interfaces;
using MyNotes.iOS.Helpers;

namespace MyNotes.iOS
{
    public class Bootstrapper_iOS
    {
        public static void Initialize()
        {
            InstanceFactory.Initialize();

            App.RegisterTypes();
            RegisterTypes();

            InstanceFactory.Build();
        }

        public static void RegisterTypes()
        {
            InstanceFactory.Register<IDeviceInfoHelper, DeviceInfoHelper_iOS>();
        }
    }
}
