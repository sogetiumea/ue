﻿using System;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Xamarin.Forms;

[assembly: ExportRenderer (typeof(Button), typeof(MyNotes.iOS.ButtonRenderer_iOS))]
namespace MyNotes.iOS
{
	public class ButtonRenderer_iOS : ButtonRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged (e);

			if (e.NewElement != null && Control != null) 
			{
				Control.BackgroundColor = UIColor.FromRGB(240, 240, 240);
			}
		}
	}
}

