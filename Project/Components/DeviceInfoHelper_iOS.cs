﻿using MyNotes.Core.Interfaces;
using UIKit;

namespace MyNotes.iOS.Helpers
{
    public class DeviceInfoHelper_iOS : IDeviceInfoHelper
    {
        public string GetDeviceInfo()
        {
            return "iOS (" + UIDevice.CurrentDevice.SystemVersion + ")";
        }
    }
}
