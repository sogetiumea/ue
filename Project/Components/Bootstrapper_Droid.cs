﻿
using MyNotes.Utils;
using MyNotes.Core.Interfaces;
using MyNotes.Droid.Helpers;

namespace MyNotes.Droid
{
    public class Bootstrapper_Droid
    {
        public static void Initialize()
        {
            InstanceFactory.Initialize();

            App.RegisterTypes();
            RegisterTypes();

            InstanceFactory.Build();
        }

        public static void RegisterTypes()
        {
            InstanceFactory.Register<IDeviceInfoHelper, DeviceInfoHelper_Droid>();
        }
    }
}
