﻿
using MyNotes.Utils;

namespace MyNotes.Droid
{
    public class Bootstrapper_Droid
    {
        public static void Initialize()
        {
            InstanceFactory.Initialize();

            App.RegisterTypes();
            RegisterTypes();

            InstanceFactory.Build();
        }

        public static void RegisterTypes()
        {
        }
    }
}
