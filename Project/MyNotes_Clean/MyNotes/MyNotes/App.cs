﻿
using Xamarin.Forms;

using MyNotes.Utils;

namespace MyNotes
{
    public class App : Application
    {
        public App()
        {
            RegisterViews();
            var page = new ContentPage
            {
                Content = new StackLayout
                {
                    VerticalOptions = LayoutOptions.Center,
                    Children = {
                        new Label {
                            HorizontalTextAlignment = TextAlignment.Center,
                            Text = "Welcome to Xamarin Forms!"
                        }
                    }
                }
            };
            MainPage = new NavigationPage(page);
        }

        public static void RegisterTypes()
        {
        }

        private void RegisterViews()
        {
            ViewFactory.Initialize();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
