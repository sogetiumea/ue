﻿using System;
using Autofac;
using System.Diagnostics;

namespace MyNotes.Utils
{
    public class InstanceFactory
    {
        private static IContainer container;
        private static ContainerBuilder builder;

        public static void Initialize()
        {
            builder = new ContainerBuilder();
        }

        public static void Register<TInterface, TType>(bool singelton = false)
        {
            if (builder == null)
            {
                throw new Exception("Error! No builder available");
            }

            if (singelton)
            {
                builder.RegisterType<TType>().As<TInterface>().SingleInstance();
            }
            else
            {
                builder.RegisterType<TType>().As<TInterface>();
            }
        }

        public static void Build()
        {
            InstanceFactory.container = builder.Build();
        }

        public static T Resolve<T>()
        {
            if (container == null)
            {
                throw new Exception("Error! No container available");
            }

            T instance;
            container.TryResolve<T>(out instance);

            if (instance == null)
            {
                throw new Exception("Error! Could not get instance of " + typeof(T).Name);
            }

            return instance;
        }




    }
}
